<?php

namespace Citrix\Authentication;

use Citrix\ServiceAbstract;
use Citrix\CitrixApiAware;

/**
 * Citrix Authentication
 *
 * Use this class to authenticate into Citrix APIs.
 *
 * @uses \Citrix\ServiceAbstract
 * @uses \Citrix\CitrixApiAware
 */
class Direct extends ServiceAbstract implements CitrixApiAware
{
    /**
     * Authentication URL
     * @var string
     */
    private $authorizeUrl = 'https://api.getgo.com/oauth/v2/token';

    /**
     * API key in Citrix's Developer Portal
     * @var string
     */
    private $apiKey;

    /**
     * API secret key in Citrix's Developer Portal
     * @var string
     */
    private $apiSecret;

    /**
     * Access Token
     * @var string
     */
    private $accessToken;

    /**
     * Organizer Key
     * @var int
     */
    private $organizerKey;

    /**
     * Being here bu passing the api key
     *
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct($apiKey = null, $apiSecret = null)
    {
        $this->setApiKey($apiKey);
        $this->setApiSecret($apiSecret);
    }

     /**
     * Authenticate by passing username and password. Those are
     * the same username and password that you use to login to
     * www.gotowebinar.com
     *
     * @param string $username
     * @param string $password
     * @return \Citrix\Citrix
     */
    public function auth($username, $password)
    {
        if (is_null($this->getApiKey()) || is_null($this->getApiSecret())) {
            $this->addError('Direct Authentication requires API key and Secret Key. Please provide API key and Secret Key.');
            return $this;
        }

        if (is_null($username) || is_null($password)) {
            $this->addError('Direct Authentication requires username and password. Please provide username and password.');
            return $this;
        }

        $params = array(
            'grant_type' => 'password',
            'username' => $username,
            'password' => $password
        );

        $this
            ->setUrl($this->authorizeUrl)
            ->setParams($params)
            ->sendFirstRequest(base64_encode($this->getApiKey() . ':' . $this->getApiSecret()))
            ->processResponse();

        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @param $apiSecret
     * @return $this
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;

        return $this;
    }
    
    /* (non-PHPdoc)
     * @see \Citrix\CitrixApiAware::processResponse()
     */
    public function processResponse()
    {
        $response = $this->getResponse();

        if (empty($response)) {
            return $this;
        }

        if (isset($response['msg'])) {
            $this->addError($response['int_err_code']);
            return $this;
        }

        $this->setAccessToken($response['access_token']);
        $this->setOrganizerKey($response['organizer_key']);
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param $accessToken
     * @return $this
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorizeUrl()
    {
        return $this->authorizeUrl;
    }

    /**
     * @param $authorizeUrl
     * @return $this
     */
    public function setAuthorizeUrl($authorizeUrl)
    {
        $this->authorizeUrl = $authorizeUrl;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrganizerKey()
    {
        return $this->organizerKey;
    }

    /**
     * @param $organizerKey
     * @return $this
     */
    public function setOrganizerKey($organizerKey)
    {
        $this->organizerKey = $organizerKey;

        return $this;
    }
}
